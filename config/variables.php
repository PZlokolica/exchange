<?php
/**
 * This file contains the configurable variables. The placeholders should be replaced.
 * IF there is a `variables.local.php` file it will be included, this file is not in GIT,
 * and should be used to overwrite the default values.
 *
 * Contains:
 *  - mail for notifications
 *  - currency discounts
 */

// Notification mail.
$notification_mail = '{mail}';

/**
 * Currency discounts.
 * Mapped by the currency short name as the key and the value ist the discount value in percentages / double.
 */
$currency_discounts = [
  'EUR' => 0.02,
  'GBP' => NULL,
  'JPY' => NULL,
];

/**
 * Api key for the currency exchange rates update command.
 */
$currency_api_key = 'f9694a1b9d2d608ef375bb9f64af379c';

// Load local configuration, if available.
if (file_exists(__DIR__ . '/variables.local.php')) {
  include __DIR__ . '/variables.local.php';
}