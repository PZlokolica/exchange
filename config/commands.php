<?php

/**
 * A list of commands that can be executed.
 */
$commands_list = [
  'update-exchange-rates' => [
    'class' => '\Exchange\Command\UpdateExchangeRatesCommand',
    'parameters' => [],
  ],
];