<?php

// List of all the events.
$events = [
  'save_exchange_order',
];

// List of the listeners registered for every event.
$event_listeners = [
  'save_exchange_order' => [
    [
      'name' => 'after_order_action',
      'class' => '\Exchange\Listener\AfterOrderAction'
    ],
  ],
];