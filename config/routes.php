<?php
/**
 * List of the routes for the application.
 */
$routes = [
  'home' => [
    'url' => '/',
    'title' => 'Home',
    'controller' => '\Exchange\Controller\FrontController',
    'show_in_navigation' => TRUE,
  ],
  'currencies_details' => [
    'url' => '/currencies_details',
    'title' => 'Currency details',
    'controller' => '\Exchange\Controller\CurrencyDetailsController',
    'show_in_navigation' => TRUE,
  ],
  'order_list' => [
    'url' => '/order_list',
    'title' => 'Order list',
    'controller' => '\Exchange\Controller\OrderListController',
    'show_in_navigation' => TRUE,
  ],

  // API routes.
  'api_create_order' => [
    'url' => '/api/create_order',
    'controller' => '\Exchange\Controller\APISaveOrderController',
    'show_in_navigation' => FALSE,
  ],
  'api_currency_details' => [
    'url' => '/api/currencies_details',
    'controller' => '\Exchange\Controller\APICurrencyDetailsController',
    'show_in_navigation' => FALSE,
  ],
];
