<?php
/**
 * This file contains the configurable connection to the database. The placeholders should be replaced.
 * IF there is a `connections.local.php` file it will be included, this file is not in GIT,
 * and should be used to overwrite the default values.
 */

// Example of connection settings.
$connections = [
  'db_host' => 'localhost',
  'db_name' => '{database_name}',
  'db_user' => '{user_name}',
  'db_password' => '{password}',
  'db_port' => '{port}',
];

// Load local configuration, if available.
if (file_exists(__DIR__ . '/connections.local.php')) {
  include __DIR__ . '/connections.local.php';
}