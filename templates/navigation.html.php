<?php
include('header.html.php');
?>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Exchange</a>
    </div>
    <ul class="nav navbar-nav navbar-dark bg-dark">
      <?php foreach ($navigation_links as $link) {?>
        <li class="nav-item <?php $link['href'] == $_SERVER['REQUEST_URI'] ? print 'active' : NULL; ?>">
          <a class="nav-link" href="<?php print $link['href']; ?>"><?php print $link['title']; ?></a>
        </li>
      <?php } ?>
    </ul>
  </div>
</nav>
