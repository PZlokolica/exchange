<?php
include('navigation.html.php');
?>

<div class="content container-fluid">
  <h1>
    Currency purchase / exchange
  </h1>

  <form class="form">
    <div class="form-group">
      <label for="select-currencies" class="control-label">
        Please select currency you wish to buy / exchange
      </label>
      <select id="select-currencies" name="currency_id" class="form-control">
        <?php foreach ($currencies as $id => $currency) { ?>
          <option class="currency" id="<?php print $currency->name; ?>-option"
                  value="<?php print $id; ?>">
            <?php print $currency->name; ?>
          </option>
        <?php } ?>
      </select>
    </div>
    <div class="form-group">
      <label for="usd-amount" class="control-label">
        Please enter the USD amount.
      </label>
      <input type="text" id="usd-amount" name="paid_value" class="form-control currency-amount-input">
    </div>
    <div class="form-group">
      <label for="currency-amount" class="control-label">
        Please enter the selected currency amount.
      </label>
      <input type="text" id="currency-amount" name="purchased_value" class="form-control currency-amount-input">
    </div>
    <div class="form-group">
      <button type="submit" id="exchange-submit" name="exchange-submit" value="Submit" class="btn btn-default">Submit</button>
    </div>
  </form>
  <div class="modal" id="confirmation-modal">
    <div class="modal-content">
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" data-toggle="modal" data-target="#confirmation-modal" class="btn btn-default">CLOSE</button>
      </div>
    </div>
  </div>
</div>

<?php
include('footer.html.php');
?>