    <footer class="page-footer">
      <div class="container-fluid text-center">
        <div class="row">
          <div class="footer-copyright text-center py-3">
            © <?php print date('Y');?> Copyright: Predrag Zlokolica
          </div>
        </div>
      </div>
    </footer>
  </body>
</html>