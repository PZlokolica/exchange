<?php
include('navigation.html.php');
?>


<div class="content container-fluid">
  <h1>
    Currencies details
  </h1>

  <table class="table">
    <thead>
      <tr>
        <?php foreach ($column_names as $name) { ?>
          <td>
            <?php print $name; ?>
          </td>
        <?php } ?>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($currencies as $id => $currency) { ?>
        <tr>
          <td class="currency-name">
            <?php print $currency->name; ?>
          </td>
          <td class="currency-short">
            <?php print $currency->short; ?>
          </td>
          <td class="currency-exchange-rate">
            <?php print $currency->exchange_rate; ?>
          </td>
          <td class="currency-surcharge">
            <?php print $currency->surcharge; ?> %
          </td>
        </tr>
      <?php } ?>
    </tbody>
  </table>

</div>

<?php
include('footer.html.php');
?>