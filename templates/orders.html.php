<?php
include('navigation.html.php');
?>

<div class="content container-fluid">
  <h1>
    Order list
  </h1>

  <table class="table">
    <thead>
    <tr>
      <?php foreach ($column_names as $name) { ?>
        <td>
          <?php print $name; ?>
        </td>
      <?php } ?>
    </tr>
    </thead>
    <tbody>
      <?php foreach ($orders as $order) { ?>
        <tr>
          <td>
            <?php print $order->id; ?>
          </td>
          <td>
            <?php print $order->currency_name; ?>
          </td>
          <td>
            <?php print $order->paid_value; ?>
          </td>
          <td>
            <?php print $order->purchased_value; ?>
          </td>
          <td>
            <?php print $order->order_exchange_rate; ?>
          </td>
          <td>
            <?php print $order->surcharge_percentage; ?>
          </td>
          <td>
            <?php print $order->surcharge_value; ?>
          </td>
          <td>
            <?php print $order->discount_percent; ?>
          </td>
          <td>
            <?php print $order->discount_amount; ?>
          </td>
          <td>
            <?php print date('d.m.Y - H:i:s', $order->date_created); ?>
          </td>
        </tr>
      <?php } ?>
    </tbody>
  </table>

</div>

<?php
include('footer.html.php');
?>