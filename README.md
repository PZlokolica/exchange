# P.I.M Exchange

App for buying / exchanging currencies

Entry point for this app is in `public/index.php`.

Commands can be executed from `execute_command.php`, and the list of commands is in `config/commands.php`.

Example:
`php execute_command.php update-exchange-rates`

Database with the needed structure is located in the `sql` folder - `sql/exchange.sql`.
This database should be imported under the name configured in the `config/connections.php`.

# Requirements

This app needs at least php 7 to function.
This app need a MySQL database to function.
This app needs rewrite rules allowed if using NGINX. For apache there is already a `.htaccess` file but the module `mod_rewrite` needs to be active.
This app needs to have a host configured for it if it is accessed via APACHE.
Composer install is required before this app can function.
 
# Settings

There are default/placeholders for database connection in the file `config/connections.php`.
However if there is a file `connections.local.php` in the folder then the values from that file will be used.

There are default/placeholders for notification mail and the currency discount in the file `config/variables.php`.However if there is a file `connections.local.php` in the folder then the values from that file will be used.
However if there is a file `variables.local.php` in the folder then the values from that file will be used.

All files in the `config` directory that have the extension `.local.php` are excluded from the git.

# Additional info

##Components
This application has it's own basic and rudimentary:
 - Routing system
   The routes are listed in `config/routes.php` and can be extended to include new routes.
   `url` - is the parameter for the url of the page / api
   `title` - is a parameter for the page name in the navigation.
   `controller` - is a class that must implement `ControllerInterface` and serves the pages and data from the backend
   `show_in_navigation` - is a parameter that specifies if the route should appear in the navigation.
   
 - Entity manager
   A class for manipulating objects in the database.
   
 - Templating engine
   Class used for rendering templates (.php) with the passed variables.
   
 - Event dispatcher and event listeners
   A system that can dispatch events - invokes all the listeners that are registered for that event.
   This is used to hook into processes and do stuff. In this app it is used to send a mail after order for GBP.
   All of the events and the event listeners are listed in `config/event_listeners`.
   
   `$events` - a variable that list names off all the events. Registers all of the available events.
   
   `$event_listeners` - contains a list of event listeners for each event.
     - `name` the name of the listener
     - `class` the listener class, must implement `EventListenerInterface` executes the action
     
 - Command system
   A system for executing commands from the command line / terminal. Command can be executed from `execute_command.php`. All commands are listed in `config/commands.php`.
   List key is the name of the command, this name is called from the command line.
    - `class` The command class must implement `CommandInterface`
    - `parameters` Parameters for the command.
    
##JS and CSS
There are main CSS and JS files in the `public` folder: `public/css/style.css` and `public/js/base_functionality.js` and those files are always included if the 'header' template is included.
Additional CSS and JS can be added in the `public` folder and then the path should be passed to the templating engine under `included_css` and `included_js` indexes as an element in an array.
That way it will be included in the 'header' template (`templates/header.html.php`).