<?php
namespace Exchange\Command;

/**
 * Provides the interface that every command must implement.
 */
interface CommandInterface {
  /**
   * Executes the command.
   */
  public function execute();
}