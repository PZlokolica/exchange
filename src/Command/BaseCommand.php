<?php
namespace Exchange\Command;

use Exchange\Services\EntityManager;

abstract class BaseCommand implements CommandInterface {

  public $parameters;

  public function __construct($parameters = []){
    $this->parameters = $parameters;
  }

  /**
   * Function that gets the entity manager to be used in the command.
   *
   * @return \Exchange\Services\EntityManager
   */
  public function getEntityManager() {
    return new EntityManager();
  }

}