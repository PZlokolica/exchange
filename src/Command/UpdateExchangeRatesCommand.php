<?php
namespace Exchange\Command;


class UpdateExchangeRatesCommand extends BaseCommand {
  public function execute(){
    // Include the variables to get the api key.
    include('config/variables.php');

    // Execute the curl to get the data.
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "http://apilayer.net/api/live?access_key=$currency_api_key&source=USD&currencies=GBP,JPY,EUR&format=1",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "Cache-Control: no-cache",
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    // If there were errors throw an exception. If not save the data.
    if ($err) {
      throw new \Exception('There was an error retrieving the currency exchange rates!' . $err);
    }
    else {
      // Decode the response.
      $response = json_decode($response, TRUE);

      // Go trough all the response currencies and update the exchange rate.
      foreach ($response['quotes'] as $currency_short => $rate) {
        // Remove USD from the currency's short name.
        $currency_short = str_replace('USD', '', $currency_short);

        // Get the entity manager and update the currency.
        $entity_manager = $this->getEntityManager();
        $entity_manager->updateEntity('currency', ['exchange_rate' => $rate], "short = '$currency_short'");
      }

    }
  }
}