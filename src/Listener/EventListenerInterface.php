<?php
namespace Exchange\Listener;

/**
 * Interface that every event listener must implement.
 */
interface EventListenerInterface {
  /**
   * Executes the listener action on the corresponding event.
   */
  public function executeAction();
}