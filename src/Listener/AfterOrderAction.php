<?php
namespace Exchange\Listener;

/**
 * Listener for the after order save event.
 */
class AfterOrderAction extends BaseListener {
  public function executeAction() {
    // Get the data passed by the event dispatcher.
    $data = $this->getEventData();

    // If the currency is GBP send an email.
    switch ($data['currency_short']) {
      case 'GBP':
        include('../config/variables.php');
        $mail_body = 'An order for British Pound (GBP) has been made, the amount bought: ' . $data['purchased_value'];
        mail($notification_mail, 'Placed order', $mail_body);
        break;
      default:
        break;
    }
  }
}