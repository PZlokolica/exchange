<?php
namespace Exchange\Listener;

abstract class BaseListener implements EventListenerInterface {

  private $event_data;

  public function __construct(&$data) {
    $this->event_data = $data;
  }

  /**
   * Function that gets the data passed by the event dispatcher.
   *
   * @return mixed
   */
  public function getEventData() {
    return $this->event_data;
  }
}