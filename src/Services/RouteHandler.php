<?php
namespace Exchange\Services;

use Exchange\Controller\ControllerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class responsible for handling HTTP requests. Resolves routes for their controllers and returns the response.
 */
class RouteHandler {

  private $routes;

  private $current_route_name;

  public function __construct(){
    // Get all the routes and set them.
    $routes = [];
    include('../config/routes.php');
    $this->routes = $routes;
  }

  /**
   * Function that handles the requested route.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return bool|\Exception|null
   */
  public function handle(Request $request) {
    try {
      $path = $request->getPathInfo();
      $controller = $this->resolveRoute($path);

      if ($controller) {
        return $controller;
      }
      else {
        throw new \Exception('Route not found!', 404);
      }
    }
    catch (\Exception $e) {
      return $e;
    }
  }

  /**
   * Function that resolves the route and returns the controller responsible for serving the route.
   *
   * @param $path
   *
   * @return bool|null
   * @throws \Exception
   */
  public function resolveRoute($path) {
    $controller = NULL;
    // Go trough all the routes and get the configured controller for the route.
    foreach ($this->routes as $title => $route_details) {
      if ($route_details['url'] === $path) {
        $this->current_route_name = $title;

        // Instantiate the controller.
        $controller = new $route_details['controller'];

        // If the configured controller is valid break the loop.
        if ($controller instanceof ControllerInterface) {
          break;
        }
        else {
          throw new \Exception('The defined class for the route must be a controller!
          It needs to implement Exchange\Controller\ControllerInterface!
          Check the class: ' . $route_details['controller'] . '.
          Correct the route configuration!');
        }
      }
    }

    if ($controller) {
      return $controller;
    }
    else {
      return FALSE;
    }
  }
}