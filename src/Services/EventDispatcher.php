<?php
namespace Exchange\Services;



use Exchange\Listener\EventListenerInterface;

class EventDispatcher {
  // All the configured events and listeners for the events.
  private $all_events;
  private $all_listeners;

  public function __construct() {
    // Get the configuration for events and listeners for the events and set them.
    include('../config/event_listeners.php');
    $this->all_events = $events;
    $this->all_listeners = $event_listeners;
  }

  /**
   * Function that returns a list of all the listeners for the requested event.
   *
   * @param $event string
   *
   * @return array
   * @throws \Exception
   */
  public function getListenersForEvent($event) {
    if (in_array($event, $this->all_events)) {
      return $this->all_listeners[$event];
    }
    else {
      throw new \Exception('The dispatched event does not exist!');
    }
  }

  /**
   * Function that dispatches the event with the supplied data.
   *
   * Invokes all of the configured listener classes and executes the `::executeAction()` method provided by the event interface.
   *
   * @param $event string
   * @param $data mixed
   *
   * @throws \Exception
   */
  public function dispatchEvent($event, $data) {
    // Get all teh configured listeners.
    $listeners = $this->getListenersForEvent($event);

    // Go trough all the listeners and execute the action that they need to do.
    foreach ($listeners as $listener_data) {
      $listener = new $listener_data['class']($data);

      // If the listener is not a valid class throw an exception.
      if ($listener instanceof EventListenerInterface) {
        $listener->executeAction();
      }
      else {
        throw new \Exception('The configured class is not a listener! A listener must implement EventListenerInterface.');
      }
    }
  }

}