<?php
namespace Exchange\Services;

/**
 * Class responsible for rendering the requested template.
 */
class TemplatingEngine {

  const TEMPLATE_DIRECTORY = '../templates';

  /**
   * Function that renders the requested template.
   * Sets the navigation and automatically adds the main CSS and JS files.
   *
   * @param $template
   * @param array $variables
   *
   * @return string
   * @throws \Exception
   */
  public function render($template, $variables = []) {
    // Get the template route.
    $template_route = self::TEMPLATE_DIRECTORY . '/' . $template;

    // Set the navigation routes.
    $variables['navigation_links'] = $this->setNavigationRoutes();

    // Add the main CSS and JS files.
    $variables['included_css'][] = 'css/style.css';
    $variables['included_js'][] = 'js/base_functionality.js';

    if (file_exists($template_route)) {
      // Start the output buffer.
      ob_start();

      // Declare all of the defined variables.
      foreach ($variables as $key => $value) {
        ${$key} = $value;
      }

      // Include the template to render it. Then return the output buffer content and clean it.
      include ($template_route);
      return ob_get_clean();
    }
    else {
      throw new \Exception("The requested template: '$template' does not exist!");
    }
  }

  /**
   * Function that returns the links for the navigation.
   *
   * @return array
   */
  public function setNavigationRoutes() {
    include('../config/routes.php');
    $navigation_links = [];
    foreach ($routes as $name => $route) {
      if ($route['show_in_navigation']) {
        $navigation_links[] = [
          'title' => $route['title'],
          'href' => $route['url'],
        ];
      }
    }

    return $navigation_links;
  }

}