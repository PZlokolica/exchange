<?php

namespace Exchange\Services;

use Exchange\Database\Database;

class EntityManager {

  private $connection;

  public function __construct() {
    // Get the connection in order to get the entities.
    $this->connection = Database::getConnection();
  }

  /**
   * Function that loads the entities by their ID.
   *
   * @param $entity_database_table_name
   * @param $id
   *
   * @return null|object|\stdClass
   */
  public function loadEntity($entity_database_table_name, $id) {
    $entity_database_table_name = $this->connection->escape_string($entity_database_table_name);
    $entity = $this->connection->query("SELECT * FROM $entity_database_table_name WHERE id = $id LIMIT 1");
    if ($entity) {
      return $entity->fetch_object();
    }
    else {
      return NULL;
    }
  }

  /**
   * Function that loads a single property of an entity.
   *
   * @param $entity_database_table_name
   * @param $id
   * @param $property
   *
   * @return null
   */
  public function getEntityProperty($entity_database_table_name, $id, $property) {
    $entity_database_table_name = $this->connection->escape_string($entity_database_table_name);
    $property = $this->connection->escape_string($property);
    $entity = $this->connection->query("SELECT $property FROM $entity_database_table_name WHERE id = $id LIMIT 1");
    if ($entity) {
      $entity = $entity->fetch_assoc();
      return $entity[$property];
    }
    else {
      return NULL;
    }
  }

  /**
   * Function that loads all of the entities for the given name from the database.
   * Returns an array of loaded objects indexed by the entity ID.
   *
   * @param $entity_database_table_name
   *
   * @param string $order_by
   *
   * @return array
   */
  public function loadAllEntities($entity_database_table_name, $order_by = 'id DESC') {
    // Set empty entities output.
    $output = [];

    // Escape the table name.
    $entity_database_table_name = $this->connection->escape_string($entity_database_table_name);

    // Execute the query.
    $entities = $this->connection->query("SELECT * FROM $entity_database_table_name ORDER BY " . $order_by);

    // If there are no errors set the output with all the entities.
    if ($entities) {
      while ($data = $entities->fetch_object()) {
        $output[$data->id] = $data;
      }
    }

    return $output;
  }

  /**
   * Function that creates entities from the supplied entity name and values.
   *
   * Values parameter has to be an array that consists of an array of entity values, that way
   * multiple entities can be inserted in a single query.
   *
   * @param $entity_database_table_name
   * @param array $values
   *
   * @throws \Exception
   */
  public function createEntities($entity_database_table_name, array $values) {
    // Check if there were supplied values.
    if (empty($values)) {
      throw new \Exception('There were no values supplied!');
    }

    // Escape the table name.
    $entity_database_table_name = $this->connection->escape_string($entity_database_table_name);

    // Set empty insert columns.
    $insert_columns = [];

    // Set empty insert values.
    $insert_values = [];

    // Go trough all the supplied values.
    foreach ($values as $key => $entity_values) {
      // Go trough all the supplied values.
      foreach ($entity_values as $column => $value) {
        // Escape strings.
        if (is_string($value)) {
          $value = $this->connection->escape_string($value);
        }
        $column = $this->connection->escape_string($column);

        // Set the insert column.
        if (!in_array($column, $insert_columns)) {
          $insert_columns[] = $column;
        }

        // Set the insert values for the single insert.
        $insert_values[$key][] = $value;
      }

      // Create a string that can be used in a query.
      $insert_values[$key] = '(' . implode(', ', $insert_values[$key]) . ')';
    }

    // Create a string for the insert columns.
    $insert_columns = implode(', ', $insert_columns);

    // Create a string for the multiple insert values.
    $insert_values = implode(', ', $insert_values);

    // Finish teh query.
    $query = "INSERT INTO $entity_database_table_name ($insert_columns) VALUES $insert_values";

    // Start a transaction and try to execute the insert.
    $this->connection->begin_transaction();
    try {
      $result = $this->connection->query($query);

      // If the result was false throw an exception with the error data.
      if (!$result) {
        throw new \Exception('There was an error inserting data. Error: ' . $this->connection->error);
      }

      $this->connection->commit();
    }
    catch (\Exception $e) {
      $this->connection->rollback();
      throw $e;
    }
  }

  /**
   * Function that updates the entity with provided values and for the condition as a string of SQL conditions.
   *
   * @param $entity_database_table_name
   * @param array $field_values
   * @param string $condition
   *
   * @throws \Exception
   */
  public function updateEntity($entity_database_table_name, array $field_values, $condition = '') {
    // Escape the table name, and the table conditions.
    $entity_database_table_name = $this->connection->escape_string($entity_database_table_name);

    $fields_for_update = [];

    // Parse the fields that need to be updated and values for hose fields.
    foreach ($field_values as $field => $value) {
      // Escape the column names and the values if they are strings.
      $field = $this->connection->escape_string($field);
      if (is_string($value)) {
        $value = "'" . $this->connection->escape_string($value) . "'";
      }

      // Create the string for `SET` part of the query.
      $fields_for_update[] = $field . ' = ' . $value;
    }

    // Implode multiple fields for the `SET` part of the query.
    $fields_for_update = implode(', ', $fields_for_update);

    // Start a transaction and try to execute the insert.
    $this->connection->begin_transaction();
    try {
      $result = $this->connection->query("UPDATE $entity_database_table_name SET $fields_for_update WHERE $condition");

      // If the result was false throw an exception with the error data.
      if (!$result) {
        throw new \Exception('There was an error updating data. Error: ' . $this->connection->error);
      }

      $this->connection->commit();
    }
    catch (\Exception $e) {
      $this->connection->rollback();
      throw $e;
    }
  }

}