<?php
namespace Exchange\Database;

/**
 * Class that is used to connect to the database.
 */
class Database {

  // Holds the connection for the singleton implementation.
  private static $instance = NULL;

  private $connection;

  private function __construct($connection_name = NULL) {
    // Get the connections data
    global $connections;
    // Set the parameters.
    $mysql_server = $connection_name ? $connections[$connection_name]['db_host'] : $connections['db_host'];
    $mysql_user = $connection_name ? $connections[$connection_name]['db_user'] : $connections['db_user'];
    $mysql_pass = $connection_name ? $connections[$connection_name]['db_password'] : $connections['db_password'];
    $mysql_database = $connection_name ? $connections[$connection_name]['db_name'] : $connections['db_name'];
    // Try connecting to the database.
    try {
      $conn = new \mysqli($mysql_server, $mysql_user, $mysql_pass, $mysql_database);
      $conn->set_charset('utf8');
      $this->connection = $conn;
    }
    catch (\Exception $e) {
      throw $e;
    }
  }

  /**
   * Function for getting the specified mysql connection.
   *
   * @param $connection_name  String  Specify which database to connect to. If it is not supplied the default one will be selected.
   * @return \mysqli
   * @throws \Exception
   */
  static public function getConnection($connection_name = NULL) {
    // Check if the connection has been made. If not create it.
    if (self::$instance === NULL) {
      self::$instance = new Database($connection_name);
    }

    return self::$instance->connection;
  }

  public function __destruct() {
    $this->connection->close();
  }
}