<?php
namespace Exchange\Controller;

use Symfony\Component\HttpFoundation\Response;

/**
 * Controller that serves the main page wit the exchange form.
 */
class FrontController extends BaseController {

  public function returnResponse() {
    // Get all the system currencies.
    $currencies = $this->manager->loadAllEntities('currency');

    // Render the page template and return a response.
    $html = $this->templating_engine->render('front_page.html.php', ['currencies' => $currencies]);
    $response = new Response($html);
    return $response;
  }

}