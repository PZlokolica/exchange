<?php
namespace Exchange\Controller;

use Symfony\Component\HttpFoundation\Response;

/**
 * Controller that lists all of the created orders.
 */
class OrderListController extends BaseController {
  public function returnResponse() {
    // Get all orders and currencies.
    $all_orders = $this->manager->loadAllEntities('exchange_order');
    $all_currencies = $this->manager->loadAllEntities('currency');

    // Add currency names to the output.
    foreach ($all_orders as $id => $order) {
      $currency = $all_currencies[$order->currency_id];
      $order->currency_name = $currency->name . ' - ' . $currency->short;
    }

    // Set column names for the table.
    $column_names = [
      'Id',
      'Currency',
      'Paid value',
      'Purchased value',
      'Exchange rate',
      'Surcharge percentage',
      'Surcharge value',
      'Discount percentage',
      'Discount value',
      'Order time'
    ];

    // Render HTML and return a response.
    $html = $this->templating_engine->render('orders.html.php', ['column_names' => $column_names, 'orders' => $all_orders]);

    return new Response($html);
  }
}