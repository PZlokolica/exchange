<?php
namespace Exchange\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

class APICurrencyDetailsController extends BaseController {
  public function returnResponse() {
    // Get the id if it is set.
    $id = $this->request->query->get('id');

    // If it is set load the requested currency if not load all.
    if ($id) {
      $result = $this->manager->loadEntity('currency', $id);
    }
    else {
      $result = $this->manager->loadAllEntities('currency');
    }

    // Set the response data.
    $response = new \stdClass();

    $response->message = 'Successfully retrieved the requested currency / currencies.';
    $response->data = $result;

    return new JsonResponse($response);
  }
}