<?php
namespace Exchange\Controller;

use Exchange\Services\EntityManager;
use Exchange\Services\TemplatingEngine;
use Symfony\Component\HttpFoundation\Request;

/**
 * Base controller class that loads the entity manager and templating engine.
 */
abstract class BaseController implements ControllerInterface {

  protected $manager;

  protected $templating_engine;

  protected $request;

  public function __construct() {
    $this->manager = new EntityManager();
    $this->templating_engine = new TemplatingEngine();
    $this->request = Request::createFromGlobals();
  }
}