<?php
namespace Exchange\Controller;

use Symfony\Component\HttpFoundation\Response;

/**
 * Controller that returns a page with the current currencies exchange rates.
 */
class CurrencyDetailsController extends BaseController {
  public function returnResponse() {
    // Get all the system currencies.
    $currencies = $this->manager->loadAllEntities('currency');

    // Set the table column names.
    $column_names = [
      'Name',
      'Short',
      'Exchange rate',
      'Surcharge'
    ];

    // Render the page and return the response.
    $html = $this->templating_engine->render('currencies_details.html.php', [
      'currencies' => $currencies,
      'column_names' => $column_names
    ]);

    return new Response($html);
  }
}