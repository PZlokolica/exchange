<?php
namespace Exchange\Controller;

interface ControllerInterface {
  /**
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function returnResponse();
}