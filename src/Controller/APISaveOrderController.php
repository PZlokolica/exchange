<?php
namespace Exchange\Controller;

use Exchange\Services\EventDispatcher;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller that saves the order made by the user.
 */
class APISaveOrderController extends BaseController {

  public function returnResponse() {
    // Get the posted data.
    $data = $this->request->getContent();

    try {
      // If there is data process the request.
      if ($data) {
        // Decode the JSON.
        $data = json_decode($data);

        // If the data is empty throw an exception.
        if (empty($data)) {
          throw new \Exception('The request does not contain data!', 422);
        }

        // Load the currency.
        $currency = $this->manager->loadEntity('currency', $data->currency_id);

        // Get the purchased value by using the exchange rate.
        $purchased_value = $data->paid_value * $currency->exchange_rate;

        // Calculate the surcharge from the purchased value.
        $surcharge = $purchased_value * ($currency->surcharge / 100);

        // Correct the purchased value for the surcharge.
        $purchased_value = $purchased_value - $surcharge;

        // Set the insert values.
        $values = [
          'currency_id' => $data->currency_id,
          'order_exchange_rate' => $currency->exchange_rate,
          'surcharge_percentage' => $currency->surcharge,
          'surcharge_value' => $surcharge,
          'purchased_value' => $purchased_value,
          'paid_value' => $data->paid_value,
          'date_created' => time(),
        ];

        // Get the configured discount for the currency.
        $discount = $this->applyDiscount($currency->short, $data->paid_value);

        // If there is a discount add it to the values.
        if ($discount) {
          $values['discount_percent'] = $discount['discount_percent'];
          $values['discount_amount'] = $discount['discount_amount'];
          $values['paid_value'] = $data->paid_value - $discount['discount_amount'];
        }

        // Execute the insert.
        $this->manager->createEntities('exchange_order', [$values]);

        // Dispatch the event.
        $event_dispatcher = new EventDispatcher();
        $event_data = $values;
        $event_data['currency_short'] = $currency->short;
        $event_dispatcher->dispatchEvent('save_exchange_order', $event_data);

        // Set the response data.
        $response = new \stdClass();

        $response->message = 'Successfully created a new order.';
        $response->data = [
          'order_details' => $values
        ];

        return new JsonResponse($response);
      }
      else {
        throw new \Exception('The request does not contain data!', 422);
      }
    }
    catch (\Exception $e) {
      throw $e;
    }
  }

  /**
   * Calculates the discount for the purchase.
   *
   * @param $currency_short
   * @param $paid_value
   *
   * @return array|null
   */
  private function applyDiscount($currency_short, $paid_value) {
    // Include the file in order to get the configurable discount.
    include('../config/variables.php');

    // If the currency discount is set calculate the amount and return the amount and the discount percent.
    if (isset($currency_discounts[$currency_short]) && $currency_discounts[$currency_short] != NULL) {
      $discount = [
        'discount_percent' => $currency_discounts[$currency_short],
        'discount_amount' => $paid_value * $currency_discounts[$currency_short]
      ];
    }
    else {
      $discount = NULL;
    }

    return $discount;
  }

}