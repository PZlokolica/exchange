<?php
require_once __DIR__ . '/../config/connections.php';
require __DIR__ . '/../vendor/autoload.php';


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Exchange\Services\RouteHandler;

$request = Request::createFromGlobals();
$handler = new RouteHandler();

$response = $handler->handle($request);

if ($response instanceof \Exception) {
  $code = $response->getCode();
  $response = new Response($response->getMessage());
  if ($code) {
    $response->setStatusCode($code);
  }
}
else {
  $response = $response->returnResponse();
}

$response->send();