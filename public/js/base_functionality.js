// Executes the base functionality for the exchange form.
$(document).ready(function(){
  // Set the variables.
  var currencyDetails = null;
  var usdAmount = 0;
  var exchangeAmount = 0;
  var selectedCurrency = null;

  // Load the currency details,
  $.get('/api/currencies_details', function(data, status){
    currencyDetails = data.data;

    // Calculate the values and change the fields for both the inputs.
    $('#usd-amount').keyup(function(event){
      usdAmount = parseFloat($(event.target).val());

      // Check if the amount is a number.
      if (isNaN(usdAmount) && $(event.target).val() != '') {
        alert('The entered value must be a number!');
        usdAmount = 0.00;
        $('#usd-amount').val(usdAmount);
      }
      selectedCurrency = $('#select-currencies').val();
      exchangeAmount = usdAmount * parseFloat(currencyDetails[selectedCurrency].exchange_rate);
      $('#currency-amount').val(exchangeAmount);
    });

    $('#currency-amount').keyup(function(event){
      exchangeAmount = parseFloat($(event.target).val());

      // Check if the amount is a number.
      if (isNaN(exchangeAmount) && $(event.target).val() != '') {
        alert('The entered value must be a number!');
        exchangeAmount = 0.00;
        $('#currency-amount').val(exchangeAmount);
      }
      selectedCurrency = $('#select-currencies').val();
      usdAmount = exchangeAmount / parseFloat(currencyDetails[selectedCurrency].exchange_rate);
      $('#usd-amount').val(usdAmount);
    });

    // When the currency is changed reset the field values.
    $('#select-currencies').change(function(event){
      $('#currency-amount').val(0.00);
      $('#usd-amount').val(0.00);
    });

  });

  // On form submit send the exchange order to the backend.
  $('#exchange-submit').click(function(event){
    event.preventDefault();

    // Check if the values are set.
    if (usdAmount == 0 || exchangeAmount == 0) {
      alert('No currency values entered! Please enter a currency value!');
    }
    else {
      // Set the post data
      var postData = {
        "currency_id": selectedCurrency,
        "paid_value": usdAmount
      };

      postData = JSON.stringify(postData);

      // Send the request. If it is successful show the modal with the details.
      $.post('/api/create_order', postData, function(data){
        data = data.data.order_details;
        $('.modal-body').html(
          "<p>" +
          "Order has been saved with the following details:" +
          "</p>" +
          "<ul>" +
          "<li>Currency: " + currencyDetails[selectedCurrency].name + "</li>" +
          "<li>Paid value: " + data.paid_value + "</li>" +
          "<li>Purchased value: " + data.purchased_value + "</li>" +
          "<li>Exchange rate: " + data.order_exchange_rate + "</li>" +
          "<li>Surcharge percentage: " + data.surcharge_percentage + "</li>" +
          "<li>Surcharge value: " + data.surcharge_value + "</li>" +
          "<li>Discount percentage: " + (data.discount_percent ? data.discount_percent : 0) + "</li>" +
          "<li>Discount value: " + (data.discount_amount ? data.discount_amount : 0) + "</li>" +
          "</ul>"
        );
        $("#confirmation-modal").modal();
      }).fail(function() {
        alert( "An error occurred while saving the order." );
      });
    }

  });



});