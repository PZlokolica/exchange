<?php
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/config/commands.php';
require __DIR__ . '/config/connections.php';

try {
  // Check if any command has been called.
  if (isset($argv[1])) {
    // Get the name.
    $command_name = $argv[1];

    // CHeck if the command is defined.
    if (array_key_exists($command_name, $commands_list)) {
      // Get the details.
      $command_details = $commands_list[$command_name];

      // Check if the class is defined.
      if (isset($command_details['class'])) {
        $command_class = $command_details['class'];

        // Check if the class exists.
        if (!class_exists($command_class)) {
          throw new Exception('The specified class does not exist! Please correct the command configuration.');
        }

        $ordered_parameters = [];
        $supplied_arguments = $argv;
        $supplied_arguments = array_splice($supplied_arguments, 2);

        if (isset($command_details['parameters'])) {
          // Check if the function has all the parameters.
          if (count($supplied_arguments) !== count($command_details['parameters'])) {
            throw new Exception('Missing parameters!');
          }

          // Set the parameters using the order of the command parameters and
          // set the key using the order of the parameters from the command details.
          foreach ($supplied_arguments as $key => $value) {
            $ordered_parameters[$command_details['parameters'][$key]] = $value;
          }
        }

        // If there are set parameters pass them to the class constructor.
        if (!empty($ordered_parameters)) {
          $command = new $command_class($ordered_parameters);

          // Check if the class implements the command interface.
          if (!($command instanceof \Exchange\Command\CommandInterface)) {
            throw new Exception('The specified class must be an instance of command interface: \Exchange\Command\CommandInterface');
          }
        }
        else {
          $command = new $command_class();
        }

        // Execute the command.
        $command->execute();

      }
      else {
        throw new Exception('The command does not have a valid class for execution! Please correct the command configuration.');
      }
    }
    else {
      throw new Exception('The requested command does not exist! Please enter a valid command.');
    }
  }
  else {
    throw new Exception('No command specified! Please enter a command.');
  }
}
catch (Exception $e) {
  // Output the error to the user.
  print $e->getMessage() . "\n";
}